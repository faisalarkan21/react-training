import React from "react";
import { Modal, Form, Input, DatePicker } from "antd";

export class AddUsersModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: "",
      date: ""
    };
  }

  handleSubmit = () => {
    this.props.onSubmit(this.state);
  };

  onCancel = () => {
    this.setState({
      date: "",
      photos: ""
    });
    this.props.handleCancelModal();
  };

  onChangeInput = (e, id) => {
    console.log("id", id);
    this.setState(
      {
        [id]: e.target.value
      },
      () => {
        console.log(this.state);
      }
    );
  };

  onChangeDate = v => {
    this.setState({
      date: v
    });
  };

  render() {
    const props = this.props;
    return (
      <Modal
        title={props.title}
        visible={props.visible}
        onOk={this.handleSubmit}
        onCancel={this.onCancel}
      >
        <Form layout="inline">
          <Form.Item label="Photos">
            <Input
              onChange={e => this.onChangeInput(e, "photos")}
              value={this.state.photos}
            />
          </Form.Item>
          <Form.Item label="Choose Date">
            <DatePicker onChange={this.onChangeDate} value={this.state.date} />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
