import React from "react";
import { connect } from "react-redux";
import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  Modal,
  Row,
  Menu,
  Dropdown,
  Icon,
  Divider
} from "antd";
import "./App.css";
import { getUsersThunk, deleteUser, addUser } from "./action/users";
import moment from "moment";
import { ConfirmModal } from "./components/modal-confirm";
import { AddUsersModal } from "./components/modal-add-users";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0,
      dataUsers: [],
      dataPosts: [],
      modalDelete: false,
      addUser: false,
      name: "",
      email: "",
      date: "",
      modalState: "",
      editUser: false,
      addUser: false
    };
  }

  handleFetchUsers = () => {
    this.props.dispatch(getUsersThunk());
  };

  handleFetchPhotos = () => {
    this.props.dispatch(getUsersThunk());
  };

  handleOpenModal = (id = "") => {
    this.setState(
      {
        modalDelete: !this.state.modalDelete,
        id
      },
      () => {
        this.handleFetchAll();
      }
    );
  };

  handleMenuClick = (e, id = "") => {
    if (e.key === "delete") {
      this.handleOpenModalDynamic("modalDelete");
    } else if (e.key === "edit") {
      this.handleOpenModalDynamic("edit");
    }
    this.setState({
      id
    });
  };

  handleOpenModalDynamic = (id, data) => {
    console.log("id", id);
    this.setState(
      {
        [id]: !this.state[id]
      },
      () => {
        console.log(this.state);

        this.handleFetchPhotos();
      }
    );
  };

  handleOnChange = e => {
    const { inputType } = e.currentTarget.dataset;
    this.setState({
      [inputType]: e.target.value
    });
  };

  onChangeDate = (date, dateString) => {
    this.setState({
      date: moment(date)
    });
  };

  handleOk = () => {
    const { id } = this.state;

    this.props.dispatch(deleteUser(id)).then(() => {
      console.log("kena");
      this.handleOpenModalDynamic("modalDelete");
    });
  };

  handleAddUsers = ({ date, photos }) => {
    const data = {
      createdAt: date,
      photos: photos
    };

    this.props.dispatch(addUser(data)).then(() => {
      this.handleOpenModalDynamic("addUser");
    });
  };

  render() {
    const { addUser } = this.state;
    // console.log("value", addUser);
    return (
      <div className="container">
        <Row>
          <Col md={3}>
            <Button onClick={this.handleFetchUsers} type="primary">
              Fetch Users
            </Button>
          </Col>
          <Col md={3}>
            <Button
              onClick={() => this.handleOpenModalDynamic("addUser")}
              type="primary"
            >
              Add Users
            </Button>
          </Col>
        </Row>
        <ConfirmModal
          title="Confirm Modal Delete"
          visible={this.state.modalDelete}
          onOk={this.handleOk}
          onCancel={() => this.handleOpenModalDynamic("modalDelete")}
        >
          Apakah anda yakin akan menghapus ?
        </ConfirmModal>

        <AddUsersModal
          onSubmit={this.handleAddUsers}
          visible={addUser}
          handleCancelModal={() => this.handleOpenModalDynamic("addUser")}
        />
        <Row>
          <Divider />
          <Col md={12}>
            <table style={{ width: "100%" }}>
              {this.props.getPhotos.length > 0 && (
                <thead className="ant-table-thead">
                  <tr>
                    <th
                      style={{ textAlign: "center" }}
                      className="ant-table-header-column"
                    >
                      Created At
                    </th>
                    <th
                      style={{ textAlign: "center" }}
                      className="ant-table-header-column"
                    >
                      Avatar
                    </th>
                    <th
                      style={{ textAlign: "center" }}
                      className="ant-table-header-column"
                    >
                      Action
                    </th>
                  </tr>
                </thead>
              )}
              {this.props.getPhotos.map(v => (
                <tr>
                  <td style={{ textAlign: "center" }} key={v.id}>
                    {moment(v.createdAt).format("LLL")}
                  </td>
                  <td style={{ textAlign: "center" }}>
                    <img
                      alt="avatar"
                      style={{ padding: 60 }}
                      src={v.photos}
                      width={200}
                      height={200}
                    ></img>
                  </td>
                  <td style={{ textAlign: "center" }}>
                    <Dropdown
                      overlay={
                        <Menu onClick={e => this.handleMenuClick(e, v.id)}>
                          <Menu.Item key="delete">Delete</Menu.Item>
                          <Menu.Item key="edit">Edit</Menu.Item>
                        </Menu>
                      }
                    >
                      <Button>
                        Actions <Icon type="down" />
                      </Button>
                    </Dropdown>
                  </td>
                </tr>
              ))}
            </table>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    getPhotos: state.getUsers.data
  };
}

export default connect(mapStateToProps)(App);
