import axios from "axios";

export function getUsers(data) {
  return {
    type: "GET_USERS",
    data
  };
}

export function getUsersThunk() {
  return dispatch => {
    axios
      .get("https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/photos")
      .then(({ data: dataUsers }) => {
        console.log("::Data-users", dataUsers);
        dispatch(getUsers(dataUsers));
      });
  };
}


export function deleteUser(id) {
  return dispatch => {
    return axios
      .delete(`https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/photos/${id}`) // hasilnya promise
  };
}

export function addUser(data) {
  return dispatch => {
    return axios
      .post(`https://5d60ae24c2ca490014b27087.mockapi.io/api/v1/photos`, data) // hasilnya promise
  };
}